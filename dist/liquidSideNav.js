angular.module("liquidSideNav", ["ui.bootstrap","template/lqd-side-nav.html","template/lqd-item.html","template/lqd-item-group.html","template/lqd-sub-menu.html","template/lqd-sub-menu-item.html"]);

angular.module('liquidSideNav')
    .controller('LiquidSideNavCtrl', ['$scope', '$window', function ($scope, $window) {
        //TODO: EXPOSE EVENTS WHEN MENU IS OPENED, CLOSED AND SUB-MENU COLLAPSED ON AND OFF
        var slideLeftBtn = angular.element(document.querySelector("#lqd-menu-btn-slide-left")),
            pushLeftBtn = angular.element(document.querySelector("#lqd-menu-btn-push-left")),
            closeBtn = angular.element(document.querySelector("#lqd-menu-btn-close"));

        //TODO: LAUNCH THIS FUNCTION WHEN THE INPUT ARROW IS CHECKED
        $scope.collapseOnOff = function ($event) {
            var liquidMenuItem_ele = $event.currentTarget.closest("li.lqd-item-container"),
                listGroup_ele = angular.element(liquidMenuItem_ele.querySelector("ul.list-group"));

            $event.currentTarget.checked ? listGroup_ele.addClass("is-active") : listGroup_ele.removeClass("is-active");
        };

        $scope.resetMenu = function () {
            var itemGroupList = angular.element(document.querySelectorAll('.lqd-item-group'));
            angular.forEach(itemGroupList, function (value, key) {
                var lqdMenuCheckbox_ele = angular.element(value.querySelector("input.lqd-menu-checkbox")),
                    listGroup_ele = angular.element(value.querySelector("ul.list-group"));

                lqdMenuCheckbox_ele.attr('checked', false);
                if (listGroup_ele.hasClass("is-active")) {
                    listGroup_ele.removeClass("is-active");
                }
            });
        };

        $scope.showMenuSlideLeft = function ($event) {
            var lqdMenu = angular.element(document.querySelector("nav.lqd-menu")),
                maskDiv = angular.element(document.querySelector("div.lqd-menu-mask-panel")),
                body = angular.element(document.body);

            body.addClass("has-active-menu");
            lqdMenu.addClass("is-active");
            maskDiv.addClass("is-active");
        };

        $scope.showMenuPushLeft = function ($event) {
            var lqdMenuPageWrapper = angular.element(document.querySelector("div.lqd-menu-wrapper")),
                lqdMenu = angular.element(document.querySelector("nav.lqd-menu")),
                maskDiv = angular.element(document.querySelector("div.lqd-menu-mask-panel")),
                body = angular.element(document.body);

            body.addClass("has-active-menu");
            lqdMenuPageWrapper.addClass("has-push-left");
            lqdMenu.addClass("is-active");
            maskDiv.addClass("is-active");
        };

        $scope.hideMenu = function ($event) {
            var lqdMenuPageWrapper = angular.element(document.querySelector("div.lqd-menu-wrapper")),
                lqdMenu = angular.element(document.querySelector("nav.lqd-menu")),
                maskDiv = angular.element(document.querySelector("div.lqd-menu-mask-panel")),
                body = angular.element(document.body);

            body.removeClass("has-active-menu");
            if (lqdMenuPageWrapper.hasClass("has-push-left")) {
                lqdMenuPageWrapper.removeClass("has-push-left");
            }
            lqdMenu.removeClass("is-active");
            maskDiv.removeClass("is-active");
            $scope.resetMenu();
        };

        if (!!slideLeftBtn) {
            slideLeftBtn.on('click', $scope.showMenuSlideLeft);
        }

        if (!!pushLeftBtn) {
            pushLeftBtn.on('click', $scope.showMenuPushLeft);
        }

        closeBtn.on('click', $scope.hideMenu);

    }]);


angular.module('liquidSideNav')
    .directive('lqdSideNav', [function () {
        return {
            restrict: "E",
            transclude: true,
            templateUrl: function(element, attrs) {
                return attrs.templateUrl || 'template/lqd-side-nav.html';
            },
            replace: true,
            link: function ($scope, element, attrs, ctrl, transclude) {

                var itemGroupList = angular.element(document.querySelectorAll('.lqd-item-group'));

                var createMaskPanel = function () {
                    var maskEle = angular.element('<div class="lqd-menu-mask-panel"></div>');
                    angular.element(document.body).append(maskEle);
                };

                /* use this in case of push-menu*/
                var createPageWrapper = function () {
                    var firstBodyChild = angular.element(document.body.children[0]);
                    firstBodyChild.wrap('<div class="lqd-menu-wrapper"></div>');
                };

                if (itemGroupList.length > 0) {
                    angular.forEach(itemGroupList, function (value, key) {
                        var itemIdlabel = "lqd-menu-item-";
                        value.querySelector("div.lqd-item-arrow input").id = itemIdlabel + key;
                        value.querySelector("div.lqd-item-arrow label").htmlFor = itemIdlabel + key;
                    });
                }

                createMaskPanel();

                /* use this in case of push-menu*/
                createPageWrapper();
            }
        };
    }]);

angular.module('liquidSideNav')
    .directive('lqdItem', [function () {
        return {
            restrict: "E",
            templateUrl: function(element, attrs) {
                return attrs.templateUrl || 'template/lqd-item.html';
            },
            replace: true,
            transclude: true,
            link: function (scope, element, attrs, ctrl, transclude) {
            }
        };
    }]);

angular.module('liquidSideNav')
    .directive('lqdItemGroup', [function () {
        return {
            restrict: "E",
            templateUrl: function(element, attrs) {
                return attrs.templateUrl || 'template/lqd-item-group.html';
            },
            replace: true,
            transclude: true,
            link: function (scope, element, attrs, ctrl, transclude) {
            }
        };
    }]);

angular.module('liquidSideNav')
    .directive('lqdSubMenu', [function () {
        return {
            restrict: "E",
            templateUrl: function(element, attrs) {
                return attrs.templateUrl || 'template/lqd-sub-menu.html';
            },
            replace: true,
            transclude: true,
            link: function (scope, element, attrs, ctrl, transclude) {
            }
        };
    }]);

angular.module('liquidSideNav')
    .directive('lqdSubMenuItem', [function () {
        return {
            restrict: "E",
            templateUrl: function(element, attrs) {
                return attrs.templateUrl || 'template/lqd-sub-menu-item.html';
            },
            replace: true,
            transclude: true,
            link: function (scope, element, attrs) {
                var href = attrs.href;
                element.removeAttr("href");
                angular.element(element.find("a")).attr("href", href);
            }
        };
    }]);


/* LIQUID SIDE NAV */
angular.module("template/lqd-side-nav.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/lqd-side-nav.html",
        "<nav id=\"lqd-menu-push-left\" class=\"lqd-menu lqd-menu-push-left\" ng-controller=\"LiquidSideNavCtrl\">\n" +
        "   <div ng-cloak>\n" +
        "       <button id=\"lqd-menu-btn-close\"  type=\"button\" class=\"btn btn-link lqd-btn-close\" ng-model=\"singleModel\" uib-btn-checkbox btn-checkbox-true=\"1\"btn-checkbox-false=\"0\">\n" +
        "           <span class=\"fa fa-times fa-2x\"></span>\n" +
        "       </button>\n" +
        "   </div>\n" +
        "   <div style=\"clear:both;padding-top: 10%\">\n" +
        "       <span ng-transclude></span>\n" +
        "   </div>\n" +
        "</nav>\n" +
        "");
}]);

/* LIQUID ITEM*/
angular.module("template/lqd-item.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/lqd-item.html",
        "<div class=\"lqd-item\">\n" +
        "   <div class=\"lqd-item-title\"><span ng-transclude></span></div>\n" +
        "   <div class=\"lqd-item-arrow\">\n" +
        "       <input class=\"lqd-menu-checkbox\" id=\"lqd-menu-check-input\" type=\"checkbox\" ng-click=\"collapseOnOff($event)\"/>\n" +
        "       <label for=\"lqd-menu-check-input\">\n" +
        "           <span class=\"fa fa-caret-right fa-2x\"></span>\n" +
        "       </label>\n" +
        "   </div>\n" +
        "</div>\n" +
        "");
}]);


/* LIQUID ITEM GROUP */
angular.module("template/lqd-item-group.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/lqd-item-group.html",
        "<ul class=\"lqd-item-group\">\n" +
        "   <li class=\"lqd-item-container ng-clock\">\n" +
        "       <span ng-transclude></span>\n" +
        "   </li>\n" +
        "</ul>\n" +
        "");
}]);


/* LIQUID SUB MENU */
angular.module("template/lqd-sub-menu.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/lqd-sub-menu.html",
        "<ul class=\"lqd-sub-menu list-group\">\n" +
        "   <span ng-transclude></span>\n" +
        "</ul>\n" +
        "");
}]);


/* LIQUID SUB MENU ITEM */
angular.module("template/lqd-sub-menu-item.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/lqd-sub-menu-item.html",
        "<li>\n" +
        "   <a href=\"#\" class=\"list-group-item\" ng-click=\"hideMenu($event)\">\n" +
        "       <span ng-transclude></span>\n" +
        "   </a>\n" +
        "</li>\n" +
        "");
}]);
