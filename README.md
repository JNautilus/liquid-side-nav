# Liquid Side Menu Navigation Bar — Pure AngularJS Navigation Menu

This project is an angularJS directive aimed at rendering a Side Menu Navigation Bar in Bootstrap style.
The peculiarity of this widget is that it is totally AngularJs based and therefore it does not require any other dependency.
This can turn to be very useful in contexts in which Jquery is not required or even forbidden.
The present project contains a sample AngularJS application ("app" folder) and the library as well ("dist" folder).


## Getting Started

To get you started you can simply clone the SideMenuNavbar Repository and install the dependencies:

### Prerequisites

You need git to clone the SideMenuNavbar repository. You can get git from
[https://bitbucket.org/JNautilus/liquid-side-nav](https://bitbucket.org/JNautilus/liquid-side-nav).

### Install Dependencies

We have two kinds of dependencies in this project: tools and angular framework code.  The tools help
us manage and test the application.

* We get the tools we depend upon via `npm`, the [node package manager][npm].
* We get the angular code via `bower`, a [client-side code package manager][bower].

We have preconfigured `npm` to automatically run `bower` so we can simply do:

```
npm install
```

Behind the scenes this will also call `bower install`.  You should find that you have two new
folders in your project.

* `node_modules` - contains the npm packages for the tools we need
* `app/bower_components` - contains the angular framework files

*Note that the `bower_components` folder would normally be installed in the root folder but
SideMenuNavbar changes this location through the `.bowerrc` file.  Putting it in the app folder makes
it easier to serve the files by a webserver.*


### Unit Tests
Karma and Jasmine have been used for unit testing. The `karma.conf.js` file, placed in the root directive, contains the configuration 
for running tests. Unit tests are placed into `unit-tests` folder. 
In order to run unit test you can simply launch the fallowing command from the prject root:

```
karma start karma.conf.js --browsers=Chrome --single-run=false --debug
```

### Build
The build procedure is currently broken into 3 steps:

* `js minification and subsequent copy of liquidSideNav.css and liquidSideNav.min.css into the dist folder`
* `css minification and subsequent copy of liquidSideNav.js and liquidSideNav.min.js into the dist folder`
* `unit tests`

In order to run the build procedure just type from the project root the following command:

```
grunt
```

The `dist` folder contains all the necessary for the deploy. A correct use of the library is shown inside the sample app that
can be found in the app folder.
