describe('Unit test for liquid Side Nav Directive', function () {
    var $compile,
        $rootScope,
        $liquidSideNav,
        liquidSideNavTpl = "<lqd-side-nav>\n" +
                           "    <lqd-item-group>\n" +
                           "        <lqd-item>item 1</lqd-item>\n" +
                           "            <lqd-sub-menu>\n" +
                           "                <lqd-sub-menu-item href=\"#\">" + "item1 link1" + "</lqd-sub-menu-item>\n" +
                           "                <lqd-sub-menu-item href=\"#\">" + "item1 link2" + "</lqd-sub-menu-item>\n" +
                           "            </lqd-sub-menu>\n" +
                           "     </lqd-item-group>\n" +
                           "    <lqd-item-group>\n" +
                           "        <lqd-item>item 2</lqd-item>\n" +
                           "            <lqd-sub-menu>\n" +
                           "                <lqd-sub-menu-item href=\"#\">" + "item2 link1" + "</lqd-sub-menu-item>\n" +
                           "                <lqd-sub-menu-item href=\"#\">" + "item2 link2" + "</lqd-sub-menu-item>\n" +
                           "            </lqd-sub-menu>\n" +
                           "     </lqd-item-group>\n" +
                           "</lqd-side-nav>\n";


    // Load the myApp module, which contains the directive
    beforeEach(module('liquidSideNav'));

    // Store references to $rootScope and $compile
    // so they are available to all tests in this describe block
    beforeEach(inject(function (_$compile_, _$rootScope_) {
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $compile = _$compile_;
        $rootScope = _$rootScope_;
        var element = $compile(liquidSideNavTpl)($rootScope);
        $rootScope.$digest();
        $liquidSideNav = element[0];
    }));


    it("It should contain a close button with proper id", function() {
        var id= angular.element($liquidSideNav.querySelectorAll("button")).attr("id");
        expect(id).toEqual("lqd-menu-btn-close");
    });


    it("It should contain the proper number of menu items", function() {
        var menuItems= angular.element($liquidSideNav.querySelectorAll(".lqd-item-group"));
        expect(menuItems.length).toEqual(2);
    });


    it("Each item should contain 2 sub menu items", function() {
        var $menuItems= angular.element($liquidSideNav.querySelectorAll(".lqd-item-group"));
        angular.forEach($menuItems, function(value, key) {
            var subMenuItems=value.querySelectorAll(".lqd-sub-menu.list-group li");
            expect(subMenuItems.length).toEqual(2);
        });
    });


});