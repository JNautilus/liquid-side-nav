//Define an angular module for our app
var sampleApp = angular.module('sampleApp', ['ngRoute','liquidSideNav']);

//Define Routing for app
sampleApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/pictures', {
                templateUrl: 'templates/documents.tmpl.html',
                controller: 'PicturesPageCrtl'
            }).
            when('/documents', {
                templateUrl: 'templates/music.tmpl.html',
                controller: 'DocumentsPageCrtl'
            }).
            when('/music', {
                templateUrl: 'templates/pictures.tmpl.html',
                controller: 'MusicPageCrtl'
            }).
            when('/videos', {
                templateUrl: 'templates/videos.tmpl.html',
                controller: 'VideosPageCrtl'
            }).
            otherwise({
                redirectTo: '/pictures'
            });
    }]);


sampleApp.controller('PicturesPageCrtl', function($scope) {
    $scope.welcomeMessage = 'Welcome to Pictures Page!!  ';
});


sampleApp.controller('DocumentsPageCrtl', function($scope) {
    $scope.welcomeMessage = 'Welcome to Documents Page!!  ';
});

sampleApp.controller('MusicPageCrtl', function($scope) {
    $scope.welcomeMessage = 'Welcome to Music Page!!  ';
});


sampleApp.controller('VideosPageCrtl', function($scope) {
    $scope.welcomeMessage = 'Welcome to Videos Page!!  ';
});